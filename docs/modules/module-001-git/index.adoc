ifdef::backend-pdf[]
:imagesdir: {moduledir}/assets/images
endif::[]

include::pages/index.adoc[]

include::pages/git_partie01_create-repo.adoc[leveloffset=+1]

include::pages/git_partie02_premier-ajout-code.adoc[leveloffset=+1]

include::pages/git_partie03_un-commit-plus-complexe.adoc[leveloffset=+1]

include::pages/git_partie04_les-commits-et-les-branches.adoc[leveloffset=+1]

include::pages/git_partie05_fusionner-des-branches.adoc[leveloffset=+1]

include::pages/git_partie06_une-fusion-de-branches-echoue.adoc[leveloffset=+1]
